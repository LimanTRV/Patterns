package bridges;

/**
 * Created by Roman Taranov on 04.05.2017.
 */
public class Inspector
{
	private BasicSocialWeb vk;
	private BasicSocialWeb facebook;

	public Inspector(BasicSocialWeb vk, BasicSocialWeb facebook)
	{
		this.vk = vk;
		this.facebook = facebook;
	}

	public String getDetailingVK()
	{
		return (vk.getHistoryPays()).toString();
	}

	public String getDetailingFacebook()
	{
		return (facebook.getHistoryPays()).toString();
	}
}
