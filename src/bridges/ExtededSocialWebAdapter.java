package bridges;

import java.util.List;
import java.util.Vector;

/**
 * Created by Roman Taranov on 04.05.2017.
 */
public class ExtededSocialWebAdapter implements ExtendedSocialWeb
{
	private BasicSocialWeb extendedSocialWeb;

	public ExtededSocialWebAdapter(BasicSocialWeb extendedSocialWeb)
	{
		this.extendedSocialWeb = extendedSocialWeb;
	}

	@Override
	public String getHistory(int userId, String date)
	{
		if (extendedSocialWeb instanceof VKSocialWeb)
		{
			return  new VKSocialWeb().getHistory();
		}
		else if (extendedSocialWeb instanceof FacebookSocialWeb)
		{
			return new FacebookSocialWeb().getHistory(date);
		}
		return null;
	}

	@Override
	public List<Integer> getLikes(int userId, boolean showNegative, int messageId)
	{
		if (extendedSocialWeb instanceof VKSocialWeb)
		{
			return new VKSocialWeb().getLikes(userId, messageId);
		}
		else if (extendedSocialWeb instanceof FacebookSocialWeb)
		{
			return new FacebookSocialWeb().getLikes(messageId, showNegative, userId);
		}
		return null;
	}
}
