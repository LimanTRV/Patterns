package bridges;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Roman Taranov on 04.05.2017.
 */
public class BasicSocialWeb implements SocialWeb
{
	private List<Integer> historyPays = new ArrayList<>();
	private DB postrgre = new PostgreDB();
	private int money = 0;
	private List<String> notifications = Arrays.asList("hello", "hi");
	private List<String> friends = new ArrayList<>();
	{
		friends.add("Dinar");
		friends.add("Artem");
	}

	public List<Integer> getHistoryPays()
	{
		return historyPays;
	}

	@Override
	public List<String> getFriends()
	{
		return postrgre.getListOfFriends();
	}

	@Override
	public void pay(int value)
	{
		money += postrgre.getMoney() + value;
		historyPays.add(postrgre.getMoney() + value);
	}

	@Override
	public List<String> wall()
	{
		return postrgre.getWall();
	}
}
