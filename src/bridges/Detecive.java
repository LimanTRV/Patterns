package bridges;

/**
 * Created by Roman Taranov on 04.05.2017.
 */
public class Detecive
{
	private SocialWeb vk;
	private SocialWeb facebook;

	public Detecive(SocialWeb vk, SocialWeb facebook)
	{
		this.vk = vk;
		this.facebook = facebook;
	}

	public String createDossierVK()
	{
		String str = "";

		for (String s: vk.wall())
		{
			str += s + " ";
		}

		return "VK dossier: " + vk.getFriends() + " " + str;
	}

	public String createDossierFacebook()
	{
		String str = "";

		for (String s: facebook.wall())
		{
			str += s + " ";
		}

		return "Facebook dossier: " + facebook.getFriends() + " " + str;
	}
}
