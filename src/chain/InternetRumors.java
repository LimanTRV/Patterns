package chain;

/**
 * Created by Roman Taranov on 05.05.2017.
 */
public class InternetRumors extends Rumor
{
	private boolean connect = true;

	public void setConnect(boolean connect)
	{
		this.connect = connect;
	}

	@Override
	public void obs()
	{
		if (!connect)
		{
			System.out.println("Error 404!");
			return;
		}
		System.out.println("Internet said: ");
		super.obs();
	}
}
