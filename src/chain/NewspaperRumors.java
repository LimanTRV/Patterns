package chain;

/**
 * Created by Roman Taranov on 05.05.2017.
 */
public class NewspaperRumors extends Rumor
{
	private long balance = 0;

	public void setBalance(long balance)
	{
		this.balance = balance;
	}

	@Override
	public void obs()
	{
		if (balance > 10_000)
		{
			System.out.println("Rumors fail");
			return;
		}
		System.out.println("Newspaper said: ");
		super.obs();
	}
}
