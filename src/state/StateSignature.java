package state;

/**
 * Created by Roman Taranov on 05.05.2017.
 */
public class StateSignature extends State
{
	@Override
	void doAction(Context context)
	{
		System.out.println("Signature document");
		super.doAction(context);
	}

	@Override
	protected void doSend(Context context)
	{
		StateRegistered stateRegistered = new StateRegistered();
		stateRegistered.doAction(context);
	}

	@Override
	protected void doBreak(Context context)
	{
		super.doBreak(context);
	}

	@Override
	protected void doBack(Context context)
	{
		super.doBack(context);
	}
}
