package state;

import java.util.Random;

/**
 * Created by Roman Taranov on 05.05.2017.
 */
public abstract class State
{
	void doAction(Context context)
	{
		context.setState(this);

		Random random = new Random();

		switch (random.nextInt(3))
		{
		case 0:
			doSend(context);
			break;

		case 1:
			doBreak(context);
			break;

		case 2:
			doBack(context);
			break;
		}

		return;
	}

	protected void doSend(Context context){}
	protected void doBreak(Context context){}
	protected void doBack(Context context){}
}
