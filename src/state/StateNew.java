package state;

import java.util.Random;

/**
 * Created by Roman Taranov on 05.05.2017.
 */
public class StateNew extends State
{
	@Override
	public void doAction(Context context)
	{
		System.out.println("Create new document");
		super.doAction(context);
	}

	@Override
	protected void doSend(Context context)
	{
		StateSignature stateSignature = new StateSignature();
		stateSignature.doAction(context);
	}

	@Override
	protected void doBreak(Context context)
	{
		System.out.println("Cancel create document");
		return;
	}

	@Override
	protected void doBack(Context context)
	{
		System.out.println("Document is incorrect. Need correcting document.");
		doAction(context);
	}
}
