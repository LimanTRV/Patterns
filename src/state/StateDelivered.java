package state;

/**
 * Created by Roman Taranov on 05.05.2017.
 */
public class StateDelivered extends State
{
	@Override
	void doAction(Context context)
	{
		System.out.println("Document is sended");
		super.doAction(context);
	}

	@Override
	protected void doSend(Context context)
	{
		StateRegistered2 stateRegistered2 = new StateRegistered2();
		stateRegistered2.doAction(context);
	}

	@Override
	protected void doBreak(Context context)
	{
		super.doBreak(context);
	}

	@Override
	protected void doBack(Context context)
	{
		super.doBack(context);
	}
}
