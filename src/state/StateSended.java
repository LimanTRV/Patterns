package state;

/**
 * Created by Roman Taranov on 05.05.2017.
 */
public class StateSended extends State
{
	@Override
	void doAction(Context context)
	{
		System.out.println("Send process...");
		super.doAction(context);
	}

	@Override
	protected void doSend(Context context)
	{
		StateDelivered stateDelivered = new StateDelivered();
		stateDelivered.doAction(context);
	}

	@Override
	protected void doBreak(Context context)
	{
		super.doBreak(context);
	}

	@Override
	protected void doBack(Context context)
	{
		super.doBack(context);
	}
}
