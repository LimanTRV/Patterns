package state;

import java.util.Random;

/**
 * Created by Roman Taranov on 05.05.2017.
 */
public class StateSend extends State
{
	@Override
	public void doAction(Context context)
	{
		System.out.println("Document send");
		super.doAction(context);
	}

	@Override
	protected void doSend(Context context)
	{
		StateSended stateSended = new StateSended();
		stateSended.doAction(context);
	}

	@Override
	public void doBreak(Context context)
	{

	}

	@Override
	public void doBack(Context context)
	{

	}
}
