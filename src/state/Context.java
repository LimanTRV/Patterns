package state;

/**
 * Created by Roman Taranov on 05.05.2017.
 */
public class Context
{
	private State state;

	public Context()
	{
		this.state = null;
	}

	public State getState()
	{
		return state;
	}

	public void setState(State state)
	{
		this.state = state;
	}
}
