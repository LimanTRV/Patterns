package state;

/**
 * Created by Roman Taranov on 05.05.2017.
 */
public class StateRegistered extends State
{
	@Override
	void doAction(Context context)
	{
		System.out.println("Document registration");
		super.doAction(context);
	}

	@Override
	protected void doSend(Context context)
	{
		StateSend stateSend = new StateSend();
		stateSend.doAction(context);
	}

	@Override
	protected void doBreak(Context context)
	{
		super.doBreak(context);
	}

	@Override
	protected void doBack(Context context)
	{
		super.doBack(context);
	}
}
