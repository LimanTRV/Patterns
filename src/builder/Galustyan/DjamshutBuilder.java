package builder.Galustyan;

import builder.HouseBuilder;

/**
 * Created by Roman Taranov on 03.05.2017.
 */
public class DjamshutBuilder implements HouseBuilder
{
	private String basement;
	private String wall;
	private String roof;

	@Override
	public void fillBasement()
	{
		basement = "Fundament";
	}

	@Override
	public void createWalls()
	{
		wall = "Stena";
	}

	@Override
	public void createRoof()
	{
		roof = "Krisha";
	}


	public String getResult()
	{
		return "DjamshutBuilder{" +
				"basement='" + basement + '\'' +
				", wall='" + wall + '\'' +
				", roof='" + roof + '\'' +
				'}';
	}
}
