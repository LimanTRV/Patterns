package builder.Galustyan;

import builder.HouseBuilder;

/**
 * Created by Roman Taranov on 03.05.2017.
 */
public class RavshanBuilder implements HouseBuilder
{
	private String basement;
	private String wall;
	private String roof;

	@Override
	public void fillBasement()
	{
		basement = "Fundament by Ravshan";
	}

	@Override
	public void createWalls()
	{
		wall = "Stena by Ravshan";
	}

	@Override
	public void createRoof()
	{
		roof = "Krisha by Ravshan";
	}

	public String getResult()
	{
		return "RavshanBuilder{" +
				"basement='" + basement + '\'' +
				", wall='" + wall + '\'' +
				", roof='" + roof + '\'' +
				'}';
	}
}
