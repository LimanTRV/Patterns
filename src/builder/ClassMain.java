package builder;

import builder.Galustyan.DjamshutBuilder;
import builder.Svetlakov.Boss;

/**
 * Created by Roman Taranov on 03.05.2017.
 */
public class ClassMain
{
	public static void main(String[] args)
	{
		DjamshutBuilder builderDj = new DjamshutBuilder();

		Boss boss = new Boss(builderDj);
	}
}
