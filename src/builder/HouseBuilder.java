package builder;

/**
 * Created by Roman Taranov on 03.05.2017.
 */
public interface HouseBuilder
{
	void fillBasement();
	void createWalls();
	void createRoof();
	String getResult();
}
