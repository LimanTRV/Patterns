package com.company;

import bridges.*;
import chain.BabkiRumors;
import chain.InternetRumors;
import chain.NewspaperRumors;
import decorator.InternetTariff;
import decorator.MainTariff;
import decorator.RoumingTariff;
import decorator.SmsTariff;
import observer.HR;
import observer.Worker;
import strategy.Context;

public class Main {

    public static void main(String[] args)
    {
        Context context = new Context();
        context.setHasFamily(true);
        context.setHasFriends(true);
        context.setCash(10_000);

        context.celebrate();




//        BabkiRumors babki = new BabkiRumors();
//        babki.setMessage("Strings are very poppulary in Innopolice");
//        babki.setLive(false);
//
//        NewspaperRumors newspaper = new NewspaperRumors();
//        newspaper.setBalance(11000);
//
//        InternetRumors internet = new InternetRumors();
//        internet.setConnect(false);
//
//        babki.setRumor(newspaper);
//        newspaper.setRumor(internet);
//
//        babki.obs();



//        BasicSocialWeb vk = new VKSocialWeb();
//
//        BasicSocialWeb faceBook = new FacebookSocialWeb();
//        ExtendedSocialWeb extendedSocialWeb = new ExtededSocialWebAdapter(faceBook);
//
//        System.out.println(extendedSocialWeb.getHistory(1, "04.05.17"));
//        System.out.println(extendedSocialWeb.getLikes(1, true, 2));
//
//        Detecive detecive = new Detecive(vk, faceBook);
//        System.out.println(detecive.createDossierFacebook());
//        System.out.println(detecive.createDossierVK());
//
//        vk.pay(5);
//        vk.pay(15);
//        vk.pay(25);
//        faceBook.pay(25);
//        faceBook.pay(35);
//        faceBook.pay(45);
//
//        Inspector inspector = new Inspector(vk, faceBook);
//        System.out.println(inspector.getDetailingVK());
//        System.out.println(inspector.getDetailingFacebook());


//        HR hr = new HR();
//        Worker worker1 = new Worker(1);
//        Worker worker2 = new Worker(2);
//
//        hr.registerObserver(worker1);
//        hr.registerObserver(worker2);
//
//        hr.notifyAllOservers();
//
//        hr.removeObserver(worker1);
//
//        hr.notifyAllOservers();


//        MainTariff mainTariff = new MainTariff();
//
//        SmsTariff smsTariff = new SmsTariff(mainTariff);
//        InternetTariff internetTariff = new InternetTariff(smsTariff);
//        RoumingTariff roumingTariff = new RoumingTariff(internetTariff);
//
//        roumingTariff.processTariff();
    }
}
