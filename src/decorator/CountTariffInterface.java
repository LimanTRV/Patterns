package decorator;

/**
 * Created by Roman Taranov on 04.05.2017.
 */
public interface CountTariffInterface
{
	void processTariff();
}
