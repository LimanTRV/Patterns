package decorator;

/**
 * Created by Roman Taranov on 04.05.2017.
 */
public class TariffDecorator implements CountTariffInterface
{
	private CountTariffInterface tariff;

	public TariffDecorator(CountTariffInterface tariff)
	{
		this.tariff = tariff;
	}

	@Override
	public void processTariff()
	{
		tariff.processTariff();
	}
}
