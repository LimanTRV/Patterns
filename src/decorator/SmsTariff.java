package decorator;

/**
 * Created by Roman Taranov on 04.05.2017.
 */
public class SmsTariff extends TariffDecorator
{
	public SmsTariff(CountTariffInterface tariff)
	{
		super(tariff);
	}

	@Override
	public void processTariff()
	{
		System.out.println("You have 100 sms");
		super.processTariff();
	}
}
