package strategy;

/**
 * Created by Roman Taranov on 05.05.2017.
 */
public interface HolidayStrategy
{
	void celebrate();
}
