package strategy;

/**
 * Created by Roman Taranov on 05.05.2017.
 */
public class FrendsHoliday implements HolidayStrategy
{
	@Override
	public void celebrate()
	{
		System.out.println("It's a sportbar evening");
	}
}
