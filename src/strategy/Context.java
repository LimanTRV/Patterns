package strategy;

/**
 * Created by Roman Taranov on 05.05.2017.
 */
public class Context
{
	private HolidayStrategy holidayStrategy;
	private boolean hasFamily;
	private boolean hasFriends;
	private int cash;

	public void setHasFamily(boolean hasFamily)
	{
		this.hasFamily = hasFamily;
	}

	public void setHasFriends(boolean hasFriends)
	{
		this.hasFriends = hasFriends;
	}

	public void setCash(int cash)
	{
		this.cash = cash;
	}

	public void celebrate()
	{
		if (hasFamily)
		{
			holidayStrategy = new FamilyHoliday();
		}
		else if (hasFriends && cash > 5000)
		{
			holidayStrategy = new FrendsHoliday();
		}
		else
		{
			holidayStrategy = new ForeverAlongHoliday();
		}

		holidayStrategy.celebrate();
	}
}
