package strategy;

/**
 * Created by Roman Taranov on 05.05.2017.
 */
public class FamilyHoliday implements HolidayStrategy
{
	@Override
	public void celebrate()
	{
		System.out.println("It's family supper");
	}
}
