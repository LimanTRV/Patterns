package factory.beretto;

import factory.*;

/**
 * Created by Roman Taranov on 03.05.2017.
 */
public class BerettoFactory implements WeaponFactory
{
	@Override
	public Gun createGun()
	{
		return null;
	}

	@Override
	public Revolver createRevolver()
	{
		return null;
	}

	@Override
	public Rifle createRifle()
	{
		return null;
	}

	@Override
	public BFG createBFG()
	{
		return null;
	}
}
